include:
  project: '$CI_PROJECT_ROOT_NAMESPACE/sys/runner_tools'
  file:
    - 'jrsrp_vars.yml'
    - 'python.yml'

variables:
  PYTHON_VERSION_LEGACY: '3.10'
  PYTHON_VERSION_CURRENT: '3.11'
  PYTHON_VERSION_EDGE: '3.12'


workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH && $CI_OPEN_MERGE_REQUESTS
      when: never
    - if: $CI_COMMIT_BRANCH
    - if: $CI_COMMIT_TAG


stages:
  - build
  - test
  - build_doc
  - release

cache:
  key: pip-pipx-pre-commit-$CI_JOB_IMAGE
  paths:
    - .cache/pip
    - .cache/pipx
    - .cache/pre-commit



.pkginstall: &pkginstall
  # we couldn't pass the version directly, we needed to pass 'edge', 'current', 'legacy'
  # so we need to work out what that actually corresponds to
  - apt update -y 
  - apt install -y "g++" git python3-dev libpq-dev
  - |
    if [ "$PY_VERSION" == "edge" ]; then
        python_version=$PYTHON_VERSION_EDGE
    elif [ "$PY_VERSION" == "current" ]; then
        python_version=$PYTHON_VERSION_CURRENT
    elif [ "$PY_VERSION" == "legacy" ]; then
        python_version=$PYTHON_VERSION_LEGACY
    else
        echo "Unsupported PY_VERSION value. The value should be 'edge', 'current', or 'legacy'."
        exit 1
    fi
  - curl -LsSf https://astral.sh/uv/install.sh -o install.sh
  - sh install.sh
  - source $HOME/.local/bin/env
  - uv venv -p $python_version /opt/venv
  - source /opt/venv/bin/activate
  - uv pip install numpy setuptools wheel pytest pytest-cov pytest-env
  - uv pip install gdal[numpy]==$(gdal-config --version) --no-build-isolation-package=gdal
  - uv pip install .[rsc] --pre rsc
  - uv build
  - |
    cat <<EOF > pytest.ini
    [pytest]
    env =
        RSS_ENV_STATE=external
    EOF


test:
  stage: test
  image: ghcr.io/osgeo/gdal:ubuntu-small-3.8.5
  id_tokens:
    VAULT_ID_TOKEN:
      aud: ${VAULT_SERVER_URL}
  parallel:
    matrix:
      - PY_VERSION:
        - 'legacy'
        - 'current'
        - 'edge'
  rules:
    - if: $PY_VERSION == "edge"
      allow_failure: true
    - when: always
  script:
    - !reference [.get_extra_index_using_curl, script]
    - *pkginstall
    - pytest --cov=/project --junitxml=report.xml --cov-report term-missing --cov-report html:cov_html
    - mv dist dist_${PY_VERSION}
    - ls dist_${PY_VERSION}
    - mkdir -p other_dist_wheels
    - |-
      if [[ $PY_VERSION != 'current' ]]; then
        cp --no-clobber dist_${PY_VERSION}/fractionalcover3*whl other_dist_wheels
      fi
  artifacts:
    untracked: false
    expire_in: 30 days
    paths:
      - cov_html
      - dist_current/fractionalcover3*whl
      - dist_current/fractionalcover3*.tar.gz
      - other_dist_wheels/fractionalcover3*whl
    reports:
      junit: report.xml

pages:
  stage: build_doc
  image: ghcr.io/osgeo/gdal:ubuntu-small-3.8.5
  id_tokens:
    VAULT_ID_TOKEN:
      aud: ${VAULT_SERVER_URL}
  script:
    - !reference [.get_extra_index_using_curl, script]
    - export PY_VERSION="current"
    - *pkginstall
    - uv pip install -r doc-requirements.txt
    - export RSCUTILS_ENV_STATE=external
    - sphinx-build docs build/sphinx || true
    - mkdir -p public
    - cp -r build/sphinx/* public/
  artifacts:
    paths:
    - public
  only:
    - main

publish:
  stage: release
  dependencies: [test]
  image: "python:3.12"
  rules: [if: $CI_COMMIT_TAG]
  script:
    - apt-get update && apt-get install -y --no-install-recommends curl ca-certificates
    - curl -LsSf https://astral.sh/uv/install.sh --output install.sh
    - sh install.sh
    - source $HOME/.local/bin/env
    - mkdir to_upload
    - cp dist_current/fractional*whl to_upload
    - cp dist_current/fractional*tar.gz to_upload
    - cp --no-clobber other_dist_wheels/fractional*whl to_upload
    - uv publish to_upload/*
    - echo "done"


